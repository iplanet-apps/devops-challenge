FROM ruby:2.5-alpine

RUN apk add build-base && \ 
    rm -rf /var/cache/apk/*
RUN mkdir /app

WORKDIR /app
COPY Gemfile /app/
COPY Gemfile.lock /app/
RUN bundle install
COPY app/ /app

CMD ["puma"]

